package com.madhav.student.dal.repos;

import org.springframework.data.repository.CrudRepository;

import com.madhav.student.dal.entities.Student;

public interface StudentRepositries extends CrudRepository<Student, Long> {

}
