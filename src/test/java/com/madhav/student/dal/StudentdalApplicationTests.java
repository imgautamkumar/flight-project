package com.madhav.student.dal;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.madhav.student.dal.entities.Student;
import com.madhav.student.dal.repos.StudentRepositries;

@RunWith(SpringRunner.class)
@SpringBootTest

public class StudentdalApplicationTests {

	@Autowired
	private StudentRepositries sr;

	@Test
	public void testCreateStudent() {

		Student student = new Student();
		student.setName("Babul");
		student.setCourse("NEET");
		student.setFee(20000d);
		sr.save(student);
	}

	@Test
	public void testUpdateStudent() {
		Optional<Student> optionalStudent = sr.findById(7L);
		Student student = optionalStudent.get();
		student.setFee(1000d);
		sr.save(student);
	}

	@Test
	public void testFindStudentById() {
		Optional<Student> optionalstudent = sr.findById(8L);
		Student student = optionalstudent.get();
		System.out.println(student);
	}

	@Test
	public void testDeleteStudent() {
		sr.deleteById(8l);
	}
}
